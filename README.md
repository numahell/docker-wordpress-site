# Site Wordpress

Basé sur Docker, ce site sert à tester l'usage de Geotrek-widget.

## Install

```bash
cp env.dist .env
docker-compose up -d
```

Puis aller à l'adresse http://0.0.0.0:8080 pour installer le site Wordpress.
